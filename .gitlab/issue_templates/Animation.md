### Description

<!-- Summarize the expected behavior. -->

### Acceptance criteria

<!-- Add acceptance criteria details. -->
- [ ] Acceptance criteria 1
- [ ] Acceptance criteria 2

/label ~animation
/assign me
/estimate 1h 0m
/milestone
