### Description

<!-- Summarize the new configuration needed concisely. -->

### Goals

<!-- List of new functionalities or objectives to accomplish with this configuration -->
- [ ] Goal 1
- [ ] Goal 2

/label ~configuration
/assign me
/estimate 1h 0m
/milestone
