# Changelog

## v1.1.0
- ADDED links and button on-hover animation

## v1.0.1
- FIX Opinions index bug
- FIX Body overflow in responsive layout
- FIX Menu images cache

## v1.0.0
- ADDED Landing and Menu pages, with
  - Locations search
  - Contact us form
  - Menu items search
  - Opinions section 
- ADDED CI / CD configuration
- ADDED Docker configuration
- MODIFIED project text fonts

## v0.2.0
- ADDED base project and base configurations
- ADDED project text fonts
