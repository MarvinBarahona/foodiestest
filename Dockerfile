# First step: build the dist folder.
FROM node:12.16.0 as build

# Install and cache app dependencies
WORKDIR /app
COPY package.json /app/package.json
RUN npm install

# Install angular CLI and build
RUN npm install -g @angular/cli@10.0.6
COPY . /app
RUN ng build --prod --optimization --output-path dist

# Second step: run with nginx
FROM nginx:1.16.0-alpine

COPY --from=build /app/dist /usr/share/nginx/html
EXPOSE 80

# Run nginx
CMD ["nginx", "-g", "daemon off;"]
