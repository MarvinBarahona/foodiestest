# Foodies Web
This a technical test project, a Foodies website. You can check it in here: https://marvinbarahona.gitlab.io/foodiestest

## Installation
This is an Angular 10 project, to run you need to clone the repository, install the dependencies and run it using @angular/cli.

You can also run it like a Docker container using the Dockerfile on the root of the project, or download a Docker image from the [container registry](https://gitlab.com/applaudostudios/angular-challenges/marvin-barahona-28-dec-2020/container_registry)

## Development

### Tools
The tools used to develop this project are: 
- Angular IDE: [Webstorm](https://www.jetbrains.com/es-es/webstorm/)
- Git client: [Sourcetree](https://www.sourcetreeapp.com/)
- API client: [Postman](https://www.postman.com/). Download the [Postman Collection](https://gitlab.com/MarvinBarahona/foodiestest/uploads/34b0032379c29f1e9c04bb145932a068/Foodies_API.postman_collection.json) used in this project

### Main dependencies: 
- [NG Bootstrap](https://ng-bootstrap.github.io/#/home): CSS components library.
- [Open Layers](https://openlayers.org/): JS Maps library

### Commit message standard
All commit messages should have include: 
- [Conventional commits](https://www.conventionalcommits.org/en/v1.0.0/) structure.
- At least one [Gitmoji](https://gitmoji.dev/) in the commit message.
- Related to an issue or merge request.

## CI / CD pipelines
This project is configured with some pipelines in the gitlab-ci.yml configuration file: 
- To build and push a Docker image to the Giltab Docker registry on every tag push. You can check them on [the Gitlab project](https://gitlab.com/applaudostudios/angular-challenges/marvin-barahona-28-dec-2020/container_registry).
- To deploy the site on [Gitlab Pages](https://marvinbarahona.gitlab.io/foodiestest) on every push to master.
- To run a Lighthouse test on every push to master (after Gitlab Pages deploy finishes). YOu can download the results in [the Gitlab Project](https://gitlab.com/MarvinBarahona/foodiestest/-/jobs)

## Progressive Web App
The project has a [Service Worker](https://angular.io/guide/service-worker-intro) configuration, so you can install it and access it offline.
