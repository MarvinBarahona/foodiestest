import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {HttpClientModule} from '@angular/common/http';

import {AppRoutingModule} from './app-routing.module';
import {SharedModule} from './shared/shared.module';
import {LandingService} from './views/landing/services/landing.service';

import {NgbCarouselModule} from '@ng-bootstrap/ng-bootstrap';

import {AppComponent} from './app.component';
import {LandingComponent} from './views/landing/landing.component';
import {OpinionsComponent} from './views/landing/components/opinions/opinions.component';
import {LocationsComponent} from './views/landing/components/locations/locations.component';
import { ServiceWorkerModule } from '@angular/service-worker';
import { environment } from '../environments/environment';

@NgModule({
  declarations: [
    AppComponent,
    LandingComponent,
    OpinionsComponent,
    LocationsComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
    AppRoutingModule,
    SharedModule,
    NgbCarouselModule,
    ServiceWorkerModule.register('ngsw-worker.js', { enabled: environment.production })
  ],
  providers: [LandingService],
  bootstrap: [AppComponent]
})
export class AppModule {
}
