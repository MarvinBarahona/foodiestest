export class LocationItem {
  id!: number;
  name!: string;
  // tslint:disable-next-line:variable-name
  opening_time!: Date;
  // tslint:disable-next-line:variable-name
  closing_time!: Date;
  address!: string;
  latitude!: number;
  longitude!: number;
  type!: string;
}
