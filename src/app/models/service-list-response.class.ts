export interface ServiceListResponse<T> {
  data: T[];
  meta: {
    total: number;
  };
}
