import {Component, OnInit} from '@angular/core';

import {LocationItem} from '../../../../models/location.class';
import {LandingService} from '../../services/landing.service';
// OpenLayers
import {Feature, Map} from 'ol';
import {OSM, Vector as VectorSource} from 'ol/source';
import View from 'ol/View';
import {Tile as TileLayer, Vector} from 'ol/layer';
import {Point} from 'ol/geom';
import {Icon, Style} from 'ol/style';
import {fromLonLat} from 'ol/proj';
import IconAnchorUnits from 'ol/style/IconAnchorUnits';
import IconOrigin from 'ol/style/IconOrigin';

@Component({
  selector: 'app-locations',
  templateUrl: './locations.component.html',
  styleUrls: ['./locations.component.sass']
})
export class LocationsComponent implements OnInit {
  items: LocationItem[];
  loadingItems = false;

  map?: Map;

  selectedCategory: string;

  query: string;
  delaySearch?: number;

  constructor(private landingService: LandingService) {
    this.items = [];
    this.selectedCategory = 'takeaway';
    this.query = '';
  }

  ngOnInit(): void {
    this.loadItems();
    this.loadMap();
  }

  loadMap(): void {
    this.map = new Map({
      target: 'locations-map',
      layers: [
        new TileLayer({
          source: new OSM()
        })
      ],
      view: new View({
        center: fromLonLat([-89.242, 13.701]),
        zoom: 14
      })
    });
  }

  loadItems(): void {
    this.loadingItems = true;

    this.map?.getLayers().getArray().forEach((layer, index) => {
      if (index !== 0) {
        this.map?.removeLayer(layer);
      }
    });

    this.landingService.getLocation(this.selectedCategory, this.query).subscribe((r) => {
      this.items = r.data;
      this.loadingItems = false;

      this.addMarker(this.items.map(
        (item) => new Point(fromLonLat([item.longitude, item.latitude]))
      ));
    });
  }

  changeCategory(selected: string): void {
    this.selectedCategory = selected;
    this.loadItems();
  }

  // Start search with a delay.
  queryItems(): void {
    if (this.delaySearch) {
      clearTimeout(this.delaySearch);
    }

    this.loadingItems = true;
    this.delaySearch = setTimeout(() => {
      this.loadItems();
    }, 600);
  }

  addMarker(markers: Point[]): void {
    this.map?.addLayer(new Vector({
      source: new VectorSource({
        features: [...markers.map((marker) => new Feature({
          geometry: marker
        }))]
      }),
      style: new Style({
        image: new Icon({
          anchor: [0.5, 0],
          anchorXUnits: IconAnchorUnits.FRACTION,
          anchorYUnits: IconAnchorUnits.PIXELS,
          anchorOrigin: IconOrigin.BOTTOM_LEFT,
          scale: 0.3,
          src: 'https://upload.wikimedia.org/wikipedia/commons/thumb/9/93/Map_marker_font_awesome.svg/200px-Map_marker_font_awesome.svg.png'
        })
      })
    }));
  }

}
