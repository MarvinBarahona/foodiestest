import {Component, OnInit, ViewChild} from '@angular/core';
import {NgbCarousel, NgbSlideEvent} from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-opinions',
  templateUrl: './opinions.component.html',
  styleUrls: ['./opinions.component.sass']
})
export class OpinionsComponent implements OnInit {
  opinions: {title: string, message: string}[];
  currentIndex: number;

  @ViewChild('opinionsCarousel', {static : true}) carousel?: NgbCarousel;

  constructor() {
    this.opinions = [
      {
        title: 'El mejor lugar para degustar en familia y amigos!',
        message: 'Es el mejor lugar al que he venido con mi familia, la comida es rica, sirven rápido y te atienden de la mejor manera.'
      },
      {
        title: 'El mejor lugar para degustar en familia y amigos!',
        message: 'Es el mejor lugar al que he venido con mi familia, la comida es rica, sirven rápido y te atienden de la mejor manera.'
      },
      {
        title: 'El mejor lugar para degustar en familia y amigos!',
        message: 'Es el mejor lugar al que he venido con mi familia, la comida es rica, sirven rápido y te atienden de la mejor manera.'
      },
      {
        title: 'El mejor lugar para degustar en familia y amigos!',
        message: 'Es el mejor lugar al que he venido con mi familia, la comida es rica, sirven rápido y te atienden de la mejor manera.'
      },
      {
        title: 'El mejor lugar para degustar en familia y amigos!',
        message: 'Es el mejor lugar al que he venido con mi familia, la comida es rica, sirven rápido y te atienden de la mejor manera.'
      }
    ];

    this.currentIndex = 0;
  }

  ngOnInit(): void {
  }

  changeCurrentIndex(event: NgbSlideEvent): void {
    this.currentIndex = Number.parseInt(event.current.charAt(event.current.length - 1), 0) % 5;
  }
}
