import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {LandingService} from './services/landing.service';

@Component({
  selector: 'app-landing',
  templateUrl: './landing.component.html',
  styleUrls: ['./landing.component.sass']
})
export class LandingComponent implements OnInit {
  submitted = false;
  submitting = false;

  contactForm: FormGroup;

  constructor(private fb: FormBuilder, private landingService: LandingService) {
    this.contactForm = this.fb.group({
      name: ['', [Validators.required, Validators.maxLength(191)]],
      email: ['', [Validators.required, Validators.email]],
      message: ['', [Validators.required, Validators.maxLength(255)]]
    });
  }

  get nameControl(): FormControl {
    return this.contactForm.get('name') as FormControl;
  }

  get emailControl(): FormControl {
    return this.contactForm.get('email') as FormControl;
  }

  get messageControl(): FormControl {
    return this.contactForm.get('message') as FormControl;
  }

  ngOnInit(): void {
  }

  submit(): void {
    this.submitting = true;

    this.landingService.postComment(this.contactForm.value).subscribe(() => {
      this.submitted = true;
      this.submitting = false;
    });
  }

}
