import {Injectable} from '@angular/core';
import {HttpClient, HttpParams} from '@angular/common/http';

import {Observable} from 'rxjs';
import {map} from 'rxjs/operators';

import {environment} from '../../../../environments/environment';

import {ServiceListResponse} from '../../../models/service-list-response.class';
import {LocationItem} from '../../../models/location.class';

@Injectable()
export class LandingService {

  constructor(private http: HttpClient) {
  }

  postComment(body: {name: string, email: string, message: string}): Observable<void> {
    const url = environment.apiUrl + '/contact';

    return this.http.post<void>(url, body);
  }

  getLocation(type: string, query?: string): Observable<ServiceListResponse<LocationItem>> {
    let params = new HttpParams({
      fromObject: {type}
    });

    if (query) {
      params = params.append('query', query);
    }

    const url = environment.apiUrl + '/locations';

    return this.http.get<ServiceListResponse<LocationItem>>(url, {params}).pipe(
      map((r) => {
        r.data = r.data.map((item) => {
          const date = '2020-01-01T';
          item.opening_time = new Date(date + item.opening_time);
          item.closing_time = new Date(date + item.closing_time);

          return item;
        });

        return r;
      })
    );
  }
}
