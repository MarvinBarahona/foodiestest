import {Component, Input, OnInit} from '@angular/core';

import {MenuItem} from '../../../../models/menu-item.class';

@Component({
  selector: 'app-menu-item',
  templateUrl: './menu-item.component.html',
  styleUrls: ['./menu-item.component.sass']
})
export class MenuItemComponent implements OnInit {
  @Input() item?: MenuItem;

  constructor() {
  }

  ngOnInit(): void {
  }

}
