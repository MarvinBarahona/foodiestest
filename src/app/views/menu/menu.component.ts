import {Component, OnInit} from '@angular/core';

import {MenuService} from './services/menu.service';

import {MenuItem} from '../../models/menu-item.class';
import {Category} from '../../models/category.class';

@Component({
  selector: 'app-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.sass']
})
export class MenuComponent implements OnInit {
  isNavbarCollapsed = true;

  items: MenuItem[];
  loadingItems = false;
  pageIndex: number;
  pageSize: number;
  totalItems: number;

  categories: Category[];
  selectedCategory: number;
  loadingCategories = false;

  query: string;
  delaySearch?: number;

  constructor(private menuService: MenuService) {
    this.pageIndex = 1;
    this.pageSize = 12;
    this.totalItems = 0;
    this.items = [];
    this.categories = [];
    this.selectedCategory = 0;
    this.query = '';
  }

  ngOnInit(): void {
    this.loadCategories();
    this.loadItems(1);
  }

  loadCategories(): void {
    this.loadingCategories = true;
    this.menuService.getCategories().subscribe((r) => {
      this.categories = r.data;
      this.loadingCategories = false;
    });
  }

  loadItems(page?: number): void {
    if (page) {
      this.pageIndex = page;
    }

    this.loadingItems = true;
    this.menuService.getMenu(this.pageIndex, this.selectedCategory, this.query).subscribe((r) => {
      this.items = r.data;
      this.totalItems = r.meta.total;
      this.loadingItems = false;
    });
  }

  changeCategory(selected: number): void {
    this.selectedCategory = selected;
    // Always hide after select
    this.isNavbarCollapsed = true;
    this.loadItems(1);
  }

  // Start search with a delay.
  queryItems(): void {
    if (this.delaySearch) {
      clearTimeout(this.delaySearch);
    }

    this.loadingItems = true;
    this.delaySearch = setTimeout(() => {
      this.loadItems(1);
    }, 600);
  }
}
