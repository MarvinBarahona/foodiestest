import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FormsModule} from '@angular/forms';
import {HttpClientModule} from '@angular/common/http';

import {NgbCollapseModule, NgbPaginationModule} from '@ng-bootstrap/ng-bootstrap';

import {MenuRoutingModule} from './menu-routing.module';
import {SharedModule} from '../../shared/shared.module';

import {MenuService} from './services/menu.service';
import {MenuComponent} from './menu.component';
import {MenuItemComponent} from './components/menu-item/menu-item.component';

@NgModule({
  declarations: [MenuComponent, MenuItemComponent],
  imports: [
    CommonModule,
    MenuRoutingModule,
    FormsModule,
    HttpClientModule,
    SharedModule,
    NgbCollapseModule,
    NgbPaginationModule
  ],
  providers: [
    MenuService
  ]
})
export class MenuModule {
}
