import {Injectable} from '@angular/core';
import {HttpClient, HttpParams} from '@angular/common/http';
import {Observable} from 'rxjs';
import {Category} from '../../../models/category.class';
import {environment} from '../../../../environments/environment';
import {ServiceListResponse} from '../../../models/service-list-response.class';
import {MenuItem} from '../../../models/menu-item.class';

@Injectable()
export class MenuService {

  constructor(private http: HttpClient) {
  }

  getCategories(): Observable<ServiceListResponse<Category>> {
    const url = environment.apiUrl + '/categories';

    return this.http.get<ServiceListResponse<Category>>(url);
  }

  getMenu(page: number, category?: number, query?: string): Observable<ServiceListResponse<MenuItem>> {
    let params = new HttpParams({
      fromObject: {page: page.toString()}
    });

    if (category) {
      params = params.append('category', category.toString());
    }

    if (query) {
      params = params.append('query', query);
    }

    const url = environment.apiUrl + '/menu';

    return this.http.get<ServiceListResponse<MenuItem>>(url, {params});
  }
}
